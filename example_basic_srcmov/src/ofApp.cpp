#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle("ofxSrcMov-example");
	ofSetFrameRate(60); // run at 60 fps
	ofSetVerticalSync(true);
	srcMov.setup("SrcMov_1");
	gui.setup("ofxSrcMov-example");
	gui.add(&srcMov.gui);
	gui.loadFromFile("settings.xml");

}

//--------------------------------------------------------------
void ofApp::update(){
	srcMov.update();

}

//--------------------------------------------------------------
void ofApp::draw(){
	srcMov.draw(0,0, ofGetWidth(), ofGetHeight());
	gui.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
	srcMov.dragdrop(dragInfo);
}
