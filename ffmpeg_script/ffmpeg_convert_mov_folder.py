import os
import subprocess
import argparse

TRANSCODE_PROFILES = [
    {"name": "All Profiles", "resolution": None, "format": None, "codec": None, "profile": None, "tune": None, "bitrate": None, "folder_name": "All"},
    {"name": "ProRes LT", "resolution": None, "format": "mov", "codec": "prores_ks", "profile": "lt", "tune": None, "bitrate": None, "folder_name": "ProResLT"},
    {"name": "ProRes Proxy SD", "resolution": "720x486", "format": "mov", "codec": "prores_ks", "profile": "proxy", "tune": None, "bitrate": None, "folder_name": "ProResProxySD"},
    {"name": "Raspberry Pi MPEG4 SD", "resolution": "640x480", "format": "mp4", "codec": "mpeg4", "profile": None, "tune": None, "bitrate": "1000k", "folder_name": "RaspberryPi_MPEG4_SD", "two_pass": True},
    {"name": "Raspberry Pi MPEG4 HD", "resolution": "1280x720", "format": "mp4", "codec": "mpeg4", "profile": None, "tune": None, "bitrate": "2000k", "folder_name": "RaspberryPi_MPEG4_HD", "two_pass": True},
    {"name": "Raspberry Pi MPEG4 Full HD", "resolution": "1920x1080", "format": "mp4", "codec": "mpeg4", "profile": None, "tune": None, "bitrate": "2500k", "folder_name": "RaspberryPi_MPEG4_FullHD", "two_pass": True},
    {
        "name": "Raspberry Pi H.264 Full HD Two-Pass",
        "resolution": "1920x1080",
        "format": "mp4",
        "codec": "libx264",
        "profile": "high",
        "tune": "film",
        "bitrate": "3000k",
        "folder_name": "RaspberryPi_H264_FullHD_TwoPass",
        "two_pass": True
    }

]

def compress_movies(source_folder, profile_index, dry_run=False):
    if profile_index == 0:
        profiles_to_execute = TRANSCODE_PROFILES[1:]
    else:
        profiles_to_execute = [TRANSCODE_PROFILES[profile_index]]

    for profile in profiles_to_execute:
        output_folder_path = os.path.join(source_folder, profile["folder_name"])

        # Create the output folder if it doesn't exist
        if not os.path.exists(output_folder_path):
            os.makedirs(output_folder_path)

        # Loop through each file in the source folder
        for filename in os.listdir(source_folder):
            if filename.endswith(('.mp4', '.mkv', '.avi', '.mov')):
                input_file_path = os.path.join(source_folder, filename)
                output_file_name = filename.rsplit('.', 1)[0] + "_" + profile["folder_name"] + "." + profile["format"]
                output_file_path = os.path.join(output_folder_path, output_file_name)
                
                command = ['ffmpeg', '-i', input_file_path, '-y']

                if profile["resolution"]:
                    command.extend(['-vf', f'scale={profile["resolution"]}'])
                if profile["codec"]:
                    command.extend(['-c:v', profile["codec"]])
                if profile["profile"]:
                    command.extend(['-profile:v', profile["profile"]])
                if profile["tune"]:
                    command.extend(['-tune', profile["tune"]])
                if profile["bitrate"]:
                    command.extend(['-b:v', profile["bitrate"]])
                command.append(output_file_path)

                # Two-pass encoding
                if profile.get("two_pass"):
                    pass1_command = command + ['-pass', '1', '-an', '-f', 'null', '/dev/null']
                    pass2_command = command + ['-pass', '2']

                    if dry_run:
                        print("First pass:", ' '.join(pass1_command))
                        print("Second pass:", ' '.join(pass2_command))
                    else:
                        subprocess.run(pass1_command)
                        subprocess.run(pass2_command)
                else:
                    if dry_run:
                        print("Would run:", ' '.join(command))
                    else:
                        subprocess.run(command)

    print(f"Compression completed. Check the source folder for output folders.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compress movie files using ffmpeg.")
    parser.add_argument("source_folder", type=str, help="Path to the source folder containing movie files.")
    parser.add_argument("-e", "--encoding", type=int, help="Specify encoding type by number. If not provided, you'll be prompted.", default=None)
    parser.add_argument("--dry-run", action="store_true", help="Perform a dry run without actual compression. Just print the operations.")
    args = parser.parse_args()

    if args.encoding is None:
        print("Select a transcoding profile:")
        for i, profile in enumerate(TRANSCODE_PROFILES):
            print(f"{i}. {profile['name']}")
        profile_index = int(input("Enter the number corresponding to the desired profile: "))
    else:
        profile_index = args.encoding

    compress_movies(args.source_folder, profile_index, args.dry_run)
