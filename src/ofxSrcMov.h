#pragma once
#include "ofMain.h"
#include "ofxGui.h"
#include <future>
#include <chrono>

class ofxSrcMov{
public:
    ofxSrcMov();
    ~ofxSrcMov();
    
    ofxGuiGroup gui;

    ofParameter <bool> enable;
    ofParameter <float> opacity;
    ofParameter <bool> play;
    ofParameter <string> moviePath;


    void setup(string name);
    void load(string _moviePath);
    void moviePathChanged(string &s_moviePath);
    void dragdrop(ofDragInfo dragInfo);
    void update();
    void draw(float x, float y);
    void draw(float x, float y, float w, float h);
    void playChanged(bool &b_play);
    void stop();
    bool isPlaying();

private:
    ofVideoPlayer movie;
    string s_loaded_movie_path;
};