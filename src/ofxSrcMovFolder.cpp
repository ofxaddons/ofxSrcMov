#include "ofxSrcMovFolder.h"

const std::vector<std::string> SUPPORTED_EXTENSIONS = {"mp4", "mov", "avi", "mkv"};

ofxSrcMovFolder::ofxSrcMovFolder() : 
        lastMovieIndex(0), 
        previousFramePosition(-1), 
        lastKnownIndex(-1){}

ofxSrcMovFolder::~ofxSrcMovFolder() {
    currentMovie.close();
    previousMovie.close();
    nextMovie.close();
    folderPathParam.removeListener(this, &ofxSrcMovFolder::folderPathChanged);
    nextButton.removeListener(this, &ofxSrcMovFolder::nextButtonPressed);
    prevButton.removeListener(this, &ofxSrcMovFolder::prevButtonPressed);
}


void ofxSrcMovFolder::setup() {


     gui.setup("Movie Folder Player");
    gui.add(enable.set("Enable", true));
    gui.add(opacity.set("Opacity", 1, 0, 1));
    gui.add(volume.set("Volume", 1.0, 0.0, 1.0));
    volume.addListener(this, &ofxSrcMovFolder::volumeChanged);
    gui.add(play.set("Play", true));
    play.addListener(this, &ofxSrcMovFolder::playChanged);
    gui.add(folderPathParam.set("folderPath","folderpath"));
    gui.add(nextButton.set("Next"));
    gui.add(prevButton.set("Prev"));
    gui.add(movieIndex.set("index", 0, 0, 100));
    gui.add(movieFramePosition.set("frame", 0, 0, 100));
    movieIndex.addListener(this, &ofxSrcMovFolder::movieIndexChanged);
    movieFramePosition.addListener(this, &ofxSrcMovFolder::movieFramePositionChanged);
    folderPathParam.addListener(this, &ofxSrcMovFolder::folderPathChanged);
    nextButton.addListener(this, &ofxSrcMovFolder::nextButtonPressed);
    prevButton.addListener(this, &ofxSrcMovFolder::prevButtonPressed);
    
}

void ofxSrcMovFolder::indexMovieFiles(string folderPath) {
    movieFiles.clear();  // Clear the movieFiles vector

    ofDirectory dir(folderPath);
    for (const auto& ext : SUPPORTED_EXTENSIONS) {
        dir.allowExt(ext);
    }
    dir.listDir();
    dir.sort(); // 2. Sort the files

    for (int i = 0; i < dir.size(); i++) {
        movieFiles.push_back(dir.getPath(i));
    }

    movieIndex.setMax(movieFiles.size() - 1);
}


void ofxSrcMovFolder::loadMovieByIndex(int index) {
    if (movieFiles.empty()) {
        ofLogError() << "No movie files loaded!";
        return;
    }

    // Always load the current movie
    currentMovie.load(movieFiles[index]);

    // Preload the previous movie if possible
    if (index - 1 >= 0) {
        previousMovie.load(movieFiles[index - 1]);
    }

    // Preload the next movie if possible
    if (index + 1 < movieFiles.size()) {
        nextMovie.load(movieFiles[index + 1]);
    }

    lastKnownIndex = index;

    // Ensure the current movie starts playing if the play parameter is true
    if (play) {
        currentMovie.play();
    }
    currentMovie.setVolume(1.0);  // Full volume for the current movie
    previousMovie.setVolume(0.0); // Mute the previous movie
    nextMovie.setVolume(0.0);     // Mute the next movie

    // Update the maximum frame count for the GUI slider
    movieFramePosition.setMax(currentMovie.getTotalNumFrames() - 1);
}


void ofxSrcMovFolder::movieIndexChanged(int &index) {
       if (index != lastKnownIndex) {
        loadMovieByIndex(index);
        lastKnownIndex = index;  // Update the last known index
    }
}

void ofxSrcMovFolder::update() {
    if (enable) {
        currentMovie.update();
        movieFramePosition = currentMovie.getCurrentFrame();
    }
}

void ofxSrcMovFolder::draw(float x, float y) {
    if (enable) {
        ofSetColor(255, 255, 255, opacity * 255);
        currentMovie.draw(x, y);
    }
}

void ofxSrcMovFolder::draw(float x, float y, float w, float h) {
    if (enable) {
        ofSetColor(255, 255, 255, opacity * 255);
        currentMovie.draw(x, y, w, h);
    }
}

void ofxSrcMovFolder::playChanged(bool &b_play) {
    if (b_play) {
        if (!currentMovie.isPlaying()) {
            currentMovie.play();
        }
    } else {
        currentMovie.setPaused(true);  // Pause the movie instead of stopping it
    }
}


void ofxSrcMovFolder::stop() {
    currentMovie.stop();
}

bool ofxSrcMovFolder::isPlaying() {
    return currentMovie.isPlaying();
}

void ofxSrcMovFolder::movieFramePositionChanged(int &frame) {
    if (currentMovie.getCurrentFrame() != frame) {
        currentMovie.setFrame(frame);
    }
}

void ofxSrcMovFolder::dragdrop(ofDragInfo dragInfo) {
    if (!dragInfo.files.empty()) {
        string path = dragInfo.files[0]; // Get path of the first dragged item
        ofFile file(path);
        
        if (file.isDirectory()) {
            // Handle directory
            folderPathParam = path;  // Update the folder path parameter
            indexMovieFiles(path);  // Use the absolute path of the dragged folder
            movieIndex = 0; // or another default value
            loadMovieByIndex(movieIndex);
        } else {
            string extension = file.getExtension();
            ofToLower(extension); // Convert to lower case for easy comparison
            
            if (extension == "mp4" || extension == "mov" || extension == "avi" || extension == "mkv" /*... add other extensions as needed ...*/) {
                currentMovie.load(path);
                if (play) {
                    currentMovie.play();
                }
            } else {
                ofLogNotice() << "Not a valid movie file!";
            }
        }
    }
}


void ofxSrcMovFolder::folderPathChanged(string &path) {
    if (ofDirectory::doesDirectoryExist(path)) {
        indexMovieFiles(path);  // Index the movie files using the new folder path
        movieIndex = 0; // Reset to the first movie
        loadMovieByIndex(movieIndex);
    } else {
        ofLogError() << "Directory does not exist: " << path;
    }
}


void ofxSrcMovFolder::nextButtonPressed() {
    if (!movieFiles.empty()) {
        movieIndex = (movieIndex + 1) % movieFiles.size();
        int currentIndex = movieIndex.get();  // Get the actual int value from ofParameter<int>
        movieIndexChanged(currentIndex);  // Ensure the movie is loaded when the index changes
    }
}

void ofxSrcMovFolder::prevButtonPressed() {
    if (!movieFiles.empty()) {
        movieIndex = (movieIndex - 1 + movieFiles.size()) % movieFiles.size();
        int currentIndex = movieIndex.get();  // Get the actual int value from ofParameter<int>
        movieIndexChanged(currentIndex);  // Ensure the movie is loaded when the index changes
    }
}

void ofxSrcMovFolder::volumeChanged(float &newVolume) {
    currentMovie.setVolume(newVolume);
}