#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include <vector>

class ofxSrcMovFolder {
public:
    ofxSrcMovFolder();
    ~ofxSrcMovFolder();

    void setup();
    void loadMovieByIndex(int index);
    void update();
    void draw(float x, float y);
    void draw(float x, float y, float w, float h);
    void playChanged(bool &b_play);
    void stop();
    bool isPlaying();

    ofxGuiGroup gui;  // Move this to public
    
    void movieIndexChanged(int &index);  // Declare this method
    void onMovieIndexScroll(ofParameter<int> &index);

    void indexMovieFiles(string folderPath);  // Declare this method
    void dragdrop(ofDragInfo dragInfo);

    void movieFramePositionChanged(int &frame);
    void nextButtonPressed();
    void prevButtonPressed();

    void volumeChanged(float &newVolume);

private:
    //ofVideoPlayer movie;
    ofVideoPlayer currentMovie;
    ofVideoPlayer previousMovie;
    ofVideoPlayer nextMovie;
    ofParameter<bool> enable;
    ofParameter<float> opacity;
    ofParameter<bool> play;
    ofParameter<int> movieIndex;
    ofParameter<int> movieFramePosition;
    ofParameter<string> folderPathParam;
    ofParameter<void> nextButton;
    ofParameter<void> prevButton;
    ofParameter<float> volume; 
    vector<string> movieFiles;  
    int lastMovieIndex;
    int previousFramePosition;
    int lastKnownIndex;

    void folderPathChanged(string &path);

   
};