#include "ofxSrcMov.h"

ofxSrcMov::ofxSrcMov() {}

ofxSrcMov::~ofxSrcMov() 
{
    movie.close();
}

void ofxSrcMov::setup(string name) 
{
    gui.setup(name);
    gui.add(enable.set("enable",1));
    gui.add(opacity.set("opacity",1,0,1));
    gui.add(play.set("play",1));
    play.addListener(this, &ofxSrcMov::playChanged);
    gui.add(moviePath.set("moviePath", "change_in_gui.mp4"));
    moviePath.addListener(this, &ofxSrcMov::moviePathChanged);
}

void ofxSrcMov::load(string _moviePath)
{
    if (_moviePath!=s_loaded_movie_path)
    {
        movie.load(_moviePath);
        moviePath=_moviePath;
        s_loaded_movie_path=_moviePath;
        if(play)
        {
            movie.play();
        }
    }
}

void ofxSrcMov::moviePathChanged(string &s_moviePath)
{
    load(moviePath.get());
}

void ofxSrcMov::update() 
{
    if(enable)
    {
        movie.update();
    }
}

void ofxSrcMov::draw(float x, float y) 
{
    if(enable)
    {
        ofSetColor(255,255,255, opacity*255);
        movie.draw(x, y);
    }
}

void ofxSrcMov::draw(float x, float y, float w, float h) 
{
    if(enable)
    {
        ofSetColor(255,255,255, opacity*255);
        movie.draw(x, y, w, h);
    }
}



void ofxSrcMov::playChanged(bool &b_play)
{
    if(b_play)
    {
        if(!isPlaying())
        {
            movie.play();
            movie.setPaused(0);
        }
    }else{
        movie.setPaused(1);
    }
}

void ofxSrcMov::stop() 
{
    movie.stop();
}

bool ofxSrcMov::isPlaying() 
{
    return movie.isPlaying();
}

void ofxSrcMov::dragdrop(ofDragInfo dragInfo)
{
    if(!dragInfo.files.empty()) {
        string filePath = dragInfo.files[0]; // Get path of the first dragged item
        ofFile file(filePath);
        
        string extension = file.getExtension();
        ofToLower(extension); // Convert to lower case for easy comparison
        
        if (extension == "mp4" || extension == "mov" || extension == "avi" || extension == "mkv" /*... add other extensions as needed ...*/) {
            moviePath=filePath;
        } else {
            ofLog() << "Not a valid movie file!";
        }
    }
}